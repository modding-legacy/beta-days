package com.legacy.betadays;

import net.minecraft.core.BlockPos;
import net.minecraft.core.component.DataComponents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.CakeBlock;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.util.TriState;
import net.neoforged.neoforge.event.ModifyDefaultComponentsEvent;
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent;
import net.neoforged.neoforge.event.entity.living.LivingEntityUseItemEvent;
import net.neoforged.neoforge.event.entity.living.LivingExperienceDropEvent;
import net.neoforged.neoforge.event.entity.living.MobEffectEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.neoforged.neoforge.event.entity.player.PlayerXpEvent;
import net.neoforged.neoforge.event.entity.player.SweepAttackEvent;
import net.neoforged.neoforge.event.level.BlockEvent.BlockToolModificationEvent;
import net.neoforged.neoforge.event.tick.EntityTickEvent;

public class BDEvents
{
	public static final ResourceLocation MAX_SPEED_ID = BetaDays.locate("max_speed");
	public static final AttributeModifier MAX_SPEED = new AttributeModifier(MAX_SPEED_ID, Double.MAX_VALUE, Operation.ADD_VALUE);

	// EventPriority#LOWEST
	public static void modifyDefaultComponents(ModifyDefaultComponentsEvent event)
	{
		if (BDConfig.STARTUP.disableFoodStacking())
		{
			event.modifyMatching(i -> i.components().has(DataComponents.FOOD) && i != Items.COOKIE, i -> i.set(DataComponents.MAX_STACK_SIZE, 1));
			event.modify(Items.COOKIE, i -> i.set(DataComponents.MAX_STACK_SIZE, 8));
		}

		if (BDConfig.STARTUP.makeFoodStorageUnstackable())
		{
			event.modify(Items.MELON, i -> i.set(DataComponents.MAX_STACK_SIZE, 1));
			event.modify(Items.DRIED_KELP_BLOCK, i -> i.set(DataComponents.MAX_STACK_SIZE, 1));

			/*event.modifyMatching(i -> i.getDefaultInstance().is(BDItemTags.FOOD_STORAGE_UNSTACKABLES), i -> i.set(DataComponents.MAX_STACK_SIZE, 1));*/
		}
	}

	@SubscribeEvent
	public static void onPlayerUpdate(EntityTickEvent.Post event)
	{
		if (event.getEntity() instanceof Player player)
		{
			// Combat cooldown
			if (BDConfig.SERVER.disableCombatCooldown())
			{
				if (!player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(MAX_SPEED_ID))
					player.getAttribute(Attributes.ATTACK_SPEED).addTransientModifier(MAX_SPEED);
			}
			else if (player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(MAX_SPEED_ID))
				player.getAttribute(Attributes.ATTACK_SPEED).removeModifier(MAX_SPEED_ID);

			// Make food always edible, but still allow sprinting if enabled
			if (BDConfig.SERVER.hungerDisabled() && player.getFoodData().getFoodLevel() != 7)
				player.getFoodData().setFoodLevel(7);

			// Reset air when surfacing while sprinting is disabled
			if (BDConfig.SERVER.disableSwimming() && player.getAirSupply() < player.getMaxAirSupply() && !player.isEyeInFluidType(NeoForgeMod.WATER_TYPE.value()))
				player.setAirSupply(player.getMaxAirSupply());
		}
	}

	@SubscribeEvent
	public static void onSweep(SweepAttackEvent event)
	{
		if (BDConfig.SERVER.disableCombatSweep() && event.getEntity().getAttribute(Attributes.SWEEPING_DAMAGE_RATIO).getValue() <= 0)
			event.setSweeping(false);
	}

	@SubscribeEvent
	public static void onRightClickBlock(RightClickBlock event)
	{
		if (BDConfig.SERVER.hungerDisabled() && event.getUseBlock() != TriState.TRUE && event.getLevel().getBlockState(event.getPos()).getBlock() instanceof CakeBlock)
			event.getEntity().heal(2.0F);
	}

	@SubscribeEvent
	public static void onPlayerRightClick(RightClickItem event)
	{
		ItemStack item = event.getItemStack();
		Player player = event.getEntity();

		if (BDConfig.SERVER.hungerDisabled() && item.getComponents().has(DataComponents.FOOD))
		{
			FoodProperties food = item.getComponents().get(DataComponents.FOOD);

			// Prevent eating if at full health unless specifically set otherwise
			if (player.getHealth() >= player.getMaxHealth() && !food.canAlwaysEat())
			{
				event.setCanceled(true);
			}
			else if (BDConfig.STARTUP.disableFoodStacking())
			{
				// eat the item right away
				player.setItemInHand(event.getHand(), item.finishUsingItem(player.level(), player));
				player.heal(food.nutrition());

				event.setCanceled(true);
			}
		}

		if (BDConfig.SERVER.originalBow())
		{
			if (item.getItem() instanceof BowItem bow)
			{
				bow.releaseUsing(item, player.level(), player, 200);
				event.setCanceled(true);
			}
			else if (item.is(Tags.Items.TOOLS_BOW))
			{
				item.finishUsingItem(player.level(), player);
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public static void onEntityXPDrop(LivingExperienceDropEvent event)
	{
		if (BDConfig.SERVER.disableExperienceDrop())
			event.setCanceled(true);
	}

	@SubscribeEvent
	public static void onExperiencePickup(PlayerXpEvent.PickupXp event)
	{
		// Failsafe
		if (BDConfig.SERVER.disableExperienceDrop())
		{
			event.getOrb().discard();
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void onEntityDamaged(LivingDamageEvent.Pre event)
	{
		if (BDConfig.SERVER.disableCombatCooldown() && event.getSource().getDirectEntity() instanceof LivingEntity living && living.getMainHandItem().is(ItemTags.AXES))
			event.setNewDamage(event.getOriginalDamage() * 0.5F);
	}

	@SuppressWarnings("deprecation")
	@SubscribeEvent
	public static void onEffectApplication(MobEffectEvent.Applicable event)
	{
		if (BDConfig.SERVER.hungerDisabled() && event.getEffectInstance().getEffect().is(MobEffects.HUNGER))
		{
			var oldInstance = event.getEffectInstance();
			event.getEntity().addEffect(new MobEffectInstance(MobEffects.POISON, oldInstance.getDuration(), oldInstance.getAmplifier(), oldInstance.isAmbient(), oldInstance.isVisible()));
			event.setResult(MobEffectEvent.Applicable.Result.DO_NOT_APPLY);
		}
	}

	@SubscribeEvent
	public static void onItemFinishUse(LivingEntityUseItemEvent.Finish event)
	{
		if (BDConfig.SERVER.hungerDisabled())
		{
			if (!BDConfig.STARTUP.disableFoodStacking() && event.getEntity() instanceof Player player && event.getItem().getComponents().has(DataComponents.FOOD))
			{
				FoodProperties food = event.getItem().getComponents().get(DataComponents.FOOD);

				event.getItem().getItem().finishUsingItem(event.getItem(), player.level(), player);
				event.getEntity().heal(food.nutrition());
			}

			/*if (event.getItem().getItem() == Items.ROTTEN_FLESH && event.getEntity().getRandom().nextBoolean())
				event.getEntity().addEffect(new MobEffectInstance(MobEffects.POISON, 60));*/
		}
	}

	@SubscribeEvent
	public static void onToolUse(final BlockToolModificationEvent event)
	{
		if (event.getItemAbility() == ItemAbilities.HOE_TILL)
		{
			if (BDConfig.SERVER.tillSeeds() && event.getState().is(BlockTags.DIRT))
			{
				LevelAccessor level = event.getLevel();

				if (level.getRandom().nextInt(10) == 0)
				{
					BlockPos pos = event.getContext().getClickedPos();
					ItemEntity itementity = new ItemEntity(event.getPlayer().level(), pos.getX() + 0.5F, pos.getY() + 1, pos.getZ() + 0.5F, new ItemStack(Items.WHEAT_SEEDS));
					itementity.setDefaultPickUpDelay();
					level.addFreshEntity(itementity);
				}
			}
		}
	}
}