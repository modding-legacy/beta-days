package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.betadays.BDConfig;

import net.minecraft.client.player.LocalPlayer;

@Mixin(LocalPlayer.class)
public class LocalPlayerMixin
{
	@Inject(at = @At("HEAD"), method = "hasEnoughImpulseToStartSprinting", cancellable = true)
	private void hasEnoughImpulseToStartSprinting(CallbackInfoReturnable<Boolean> callback)
	{
		var player = ((LocalPlayer) (Object) this);
		// most consistant and stable way to stop sprinting without hacky FOV stuff
		if (!player.isCreative() && !player.isSpectator() && (BDConfig.SERVER.disableSprinting() && !this.isUnderWater() || BDConfig.SERVER.disableSwimming() && this.isUnderWater()))
			callback.setReturnValue(false);
	}

	@Shadow
	public boolean isUnderWater()
	{
		return false;
	}
}
