package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.betadays.BDConfig;
import com.legacy.betadays.client.DimensionChangeTexts;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;

@Mixin(ReceivingLevelScreen.class)
public class ReceivingLevelScreenMixin extends Screen
{
	protected ReceivingLevelScreenMixin(Component title)
	{
		super(title);
	}

	@Shadow
	@Final
	private ReceivingLevelScreen.Reason reason;

	@ModifyConstant(method = "render(Lnet/minecraft/client/gui/GuiGraphics;IIF)V", constant = @Constant(intValue = 50))
	private int beta_days$modifyTextHeight(int original)
	{
		if (BDConfig.CLIENT.customDimensionMessages() && DimensionChangeTexts.getText(this.reason) != null)
			return 30;

		return original;
	}

	@Inject(at = @At("TAIL"), method = "render(Lnet/minecraft/client/gui/GuiGraphics;IIF)V")
	private void render(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTick, CallbackInfo callback)
	{
		Component text = DimensionChangeTexts.getText(this.reason);
		if (text != null)
			guiGraphics.drawCenteredString(this.font, text, this.width / 2, this.height / 2 - 50, -1);
	}
}
