package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.betadays.BDConfig;

import net.minecraft.client.renderer.DimensionSpecialEffects;

@Mixin(DimensionSpecialEffects.NetherEffects.class)
public class NetherEffectsMixin
{
	@Inject(at = @At("HEAD"), method = "isFoggyAt", cancellable = true)
	private void isFoggyAt(int p_108905_, int p_108906_, CallbackInfoReturnable<Boolean> callback)
	{
		if (BDConfig.CLIENT.disableNetherFog())
			callback.setReturnValue(false);
	}
}
