package com.legacy.betadays.client;

import com.legacy.betadays.BDConfig;
import com.legacy.betadays.client.gui.BetaPanoramaRenderer;
import com.legacy.betadays.client.gui.ClassicMainMenuScreen;
import com.legacy.betadays.client.layer.OldVersionTitleLayer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.PanoramaRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.RegisterGuiLayersEvent;
import net.neoforged.neoforge.client.event.RenderGuiLayerEvent;
import net.neoforged.neoforge.client.event.ScreenEvent;
import net.neoforged.neoforge.client.event.sound.PlaySoundEvent;
import net.neoforged.neoforge.client.gui.VanillaGuiLayers;

@SuppressWarnings("resource")
public class BDClientEvents
{
	protected static final PanoramaRenderer BETA_PANORAMA = new BetaPanoramaRenderer();

	public static void registerOverlays(RegisterGuiLayersEvent event)
	{
		event.registerAboveAll(OldVersionTitleLayer.OLD_VERSION_TITLE, new OldVersionTitleLayer());
	}

	@SubscribeEvent
	public static void preInitGuiEvent(ScreenEvent.Init.Pre event)
	{
		if (BDConfig.CLIENT.enableClassicMenu())
		{
			Screen screen = event.getScreen();
			if (screen == null)
				return;

			if (screen instanceof TitleScreen)
				Screen.PANORAMA = BETA_PANORAMA;
		}
	}

	@SubscribeEvent
	public static void onOpenGui(ScreenEvent.Opening event)
	{
		if (event.getScreen() instanceof ReceivingLevelScreen)
			DimensionChangeTexts.updateKnownLevel();

		if (BDConfig.CLIENT.enableClassicMenu() && event.getScreen() != null && event.getScreen().getClass() == TitleScreen.class && !mc().isDemo())
			event.setNewScreen(new ClassicMainMenuScreen());
	}

	@SubscribeEvent
	public static void onSoundPlayed(PlaySoundEvent event)
	{
		if (BDConfig.CLIENT.disableCombatSounds() && event.getSound() != null)
		{
			ResourceLocation sound = event.getSound().getLocation();

			if (sound == SoundEvents.PLAYER_ATTACK_NODAMAGE.location() || sound == SoundEvents.PLAYER_ATTACK_WEAK.location() || sound == SoundEvents.PLAYER_ATTACK_STRONG.location() || sound == SoundEvents.PLAYER_ATTACK_CRIT.location() || sound == SoundEvents.PLAYER_ATTACK_SWEEP.location() || sound == SoundEvents.PLAYER_ATTACK_KNOCKBACK.location())
				event.setSound(null);
		}
	}

	@SubscribeEvent
	public static void renderOverlayPre(RenderGuiLayerEvent.Pre event)
	{
		ResourceLocation id = event.getName();
		Gui gui = mc().gui;
		LocalPlayer player = mc().player;

		if (id.equals(VanillaGuiLayers.EXPERIENCE_BAR))
		{
			if (BDConfig.SERVER.disableExperienceDrop())
			{
				gui.leftHeight -= 7;
				gui.rightHeight -= 7;
				event.setCanceled(true);
			}

			// Experience seems to happen before both the left and right side HUD elements.
			// This is the best place to offset them as a group.
			if (BDConfig.SERVER.hungerDisabled())
			{
				gui.rightHeight += 10;

				if (player != null && player.jumpableVehicle() != null)
				{
					gui.leftHeight += 7;
					gui.rightHeight += 7;
				}
			}
		}

		if (BDConfig.SERVER.hungerDisabled())
		{
			if (id.equals(VanillaGuiLayers.ARMOR_LEVEL))
			{
				if (player != null)
				{
					// We subtract this because it goes on the right side now.
					var baseArmor = -(gui.leftHeight + 10);
					event.getGuiGraphics().pose().pushPose();
					event.getGuiGraphics().pose().translate(101F, baseArmor + (gui.rightHeight + 10), 0);
					gui.leftHeight -= 10;

					// offset everything down if no armor is present.
					if (player.getArmorValue() <= 0)
						gui.rightHeight -= 10;
				}
			}

			if (id.equals(VanillaGuiLayers.FOOD_LEVEL))
				event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void renderOverlayPost(RenderGuiLayerEvent.Post event)
	{
		ResourceLocation id = event.getName();

		// pop the stack since we push it earlier
		if (BDConfig.SERVER.hungerDisabled() && id.equals(VanillaGuiLayers.ARMOR_LEVEL) && mc().player != null)
			event.getGuiGraphics().pose().popPose();
	}

	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}
}