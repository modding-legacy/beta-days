package com.legacy.betadays.client.gui;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.LogoRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.util.ARGB;
import net.minecraft.util.RandomSource;

/**
 * No edition label
 */
public class BetaLogoRenderer extends LogoRenderer
{
	private final boolean showEasterEgg = (double) RandomSource.create().nextFloat() < 1.0E-4;

	public BetaLogoRenderer()
	{
		super(true);
	}

	@Override
	public void renderLogo(GuiGraphics guiGraphics, int screenWidth, float transparency, int height)
	{
		guiGraphics.blit(RenderType::guiTextured, this.showEasterEgg ? EASTER_EGG_LOGO : MINECRAFT_LOGO, screenWidth / 2 - 128, height, 0.0F, 0.0F, 256, 44, 256, 64, ARGB.white(1));
	}
}
