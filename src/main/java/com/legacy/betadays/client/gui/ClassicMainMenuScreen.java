package com.legacy.betadays.client.gui;

import com.legacy.betadays.BetaDays;
import com.legacy.betadays.client.layer.OldVersionTitleLayer;

import net.minecraft.ChatFormatting;
import net.minecraft.SharedConstants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.CommonButtons;
import net.minecraft.client.gui.components.LogoRenderer;
import net.minecraft.client.gui.components.PlainTextButton;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.SplashRenderer;
import net.minecraft.client.gui.components.SpriteIconButton;
import net.minecraft.client.gui.screens.CreditsAndAttributionScreen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.gui.screens.multiplayer.JoinMultiplayerScreen;
import net.minecraft.client.gui.screens.options.AccessibilityOptionsScreen;
import net.minecraft.client.gui.screens.options.LanguageSelectScreen;
import net.minecraft.client.gui.screens.options.OptionsScreen;
import net.minecraft.client.gui.screens.packs.PackSelectionScreen;
import net.minecraft.client.gui.screens.worldselection.SelectWorldScreen;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.repository.PackRepository;
import net.neoforged.neoforge.client.ClientHooks;
import net.neoforged.neoforge.client.gui.ModListScreen;

public class ClassicMainMenuScreen extends TitleScreen
{
	private static final ResourceLocation FORGE_BUTTON_SPRITE = BetaDays.locate("icon/forge");
	private static final Component COPYRIGHT_TEXT = Component.translatable("title.credits");

	private SplashRenderer splash;

	private final LogoRenderer logoRenderer;
	private Component versionText;

	public ClassicMainMenuScreen()
	{
		this(false);
	}

	public ClassicMainMenuScreen(boolean fadeIn)
	{
		super(fadeIn);
		this.logoRenderer = new BetaLogoRenderer();
	}

	public static SpriteIconButton modsButton(int width, Button.OnPress onPress, boolean iconOnly)
	{
		return SpriteIconButton.builder(Component.translatable("fml.menu.mods"), onPress, iconOnly).width(width).sprite(FORGE_BUTTON_SPRITE, 15, 15).build();
	}

	@Override
	protected void init()
	{
		if (this.splash == null)
			this.splash = this.minecraft.getSplashManager().getSplash();

		if (this.versionText == null)
			this.versionText = OldVersionTitleLayer.getVersionName(this.minecraft).copy().withStyle(ChatFormatting.DARK_GRAY);

		this.addRenderableWidget(modsButton(20, p_344157_ -> this.minecraft.setScreen(new ModListScreen(this)), true)).setPosition(this.width - 25, 3 * 2);
		this.addRenderableWidget(CommonButtons.accessibility(20, p_344158_ -> this.minecraft.setScreen(new AccessibilityOptionsScreen(this, this.minecraft.options)), true)).setPosition(this.width - 25, 3 * 10);
		this.addRenderableWidget(CommonButtons.language(20, p_344157_ -> this.minecraft.setScreen(new LanguageSelectScreen(this, this.minecraft.options, this.minecraft.getLanguageManager())), true)).setPosition(this.width - 25, 3 * 18);

		int j = this.height / 4 + 48;
		this.addSingleplayerMultiplayerButtons(j, 24);

		this.addRenderableWidget(Button.builder(Component.translatable("resourcePack.title"), (onPress) ->
		{
			this.minecraft.setScreen(new PackSelectionScreen(this.minecraft.getResourcePackRepository(), this::updatePackList, this.minecraft.getResourcePackDirectory(), Component.translatable("resourcePack.title")));
		}).bounds(this.width / 2 - 100, j + (24 * 2), 200, 20).build());

		int bottomHeight = j + ((24 * 3) + 12);
		this.addRenderableWidget(Button.builder(Component.translatable("menu.options"), onPress -> this.minecraft.setScreen(new OptionsScreen(this, this.minecraft.options))).bounds(this.width / 2 - 100, bottomHeight, 98, 20).build());
		this.addRenderableWidget(Button.builder(Component.translatable("menu.quit"), p_280831_ -> this.minecraft.stop()).bounds(this.width / 2 + 2, bottomHeight, 98, 20).build());

		this.addRenderableWidget(new PlainTextButton(this.width - this.font.width(COPYRIGHT_TEXT) - 3, this.height - 10, this.font.width(COPYRIGHT_TEXT), 10, COPYRIGHT_TEXT, p_280834_ -> this.minecraft.setScreen(new CreditsAndAttributionScreen(this)), this.font));
	}

	@Override
	public void render(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTick)
	{
		this.renderPanorama(guiGraphics, partialTick);
		this.renderBackground(guiGraphics, mouseX, mouseY, partialTick);

		this.logoRenderer.renderLogo(guiGraphics, this.width, 1);

		for (Renderable renderable : this.renderables)
			renderable.render(guiGraphics, mouseX, mouseY, partialTick);

		int i = 1;
		ClientHooks.renderMainMenu(this, guiGraphics, this.font, this.width, this.height, i);

		if (this.splash != null && !this.minecraft.options.hideSplashTexts().get())
			this.splash.render(guiGraphics, this.width, this.font, i);

		String s = "Minecraft " + SharedConstants.getCurrentVersion().getName();
		if (this.minecraft.isDemo())
			s = s + " Demo";
		else
			s = s + ("release".equalsIgnoreCase(this.minecraft.getVersionType()) ? "" : "/" + this.minecraft.getVersionType());

		if (Minecraft.checkModStatus().shouldReportAsModified())
			s = s + I18n.get("menu.modded");

		/*BrandingControl.forEachLine(true, true, (brdline, brd) -> guiGraphics.drawString(this.font, brd, 2, this.height - (10 + brdline * (this.font.lineHeight + 1)), 16777215 | i));
		BrandingControl.forEachAboveCopyrightLine((brdline, brd) -> guiGraphics.drawString(this.font, brd, this.width - font.width(brd), this.height - (10 + (brdline + 1) * (this.font.lineHeight + 1)), 16777215 | i));*/

		if (this.versionText != null)
			guiGraphics.drawString(this.font, this.versionText, 2, 3 * 2, -1);
	}

	@Override
	public void renderBackground(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTick)
	{
		super.renderBackground(guiGraphics, mouseX, mouseY, partialTick);
	}

	@Override
	protected void renderPanorama(GuiGraphics guiGraphics, float partialTick)
	{
		super.renderPanorama(guiGraphics, partialTick);
	}

	private void addSingleplayerMultiplayerButtons(int y, int rowHeight)
	{
		this.addRenderableWidget(Button.builder(Component.translatable("menu.singleplayer"), (p_213089_1_) ->
		{
			this.minecraft.setScreen(new SelectWorldScreen(this));
		}).bounds(this.width / 2 - 100, y, 200, 20).build());
		this.addRenderableWidget(Button.builder(Component.translatable("menu.multiplayer"), (p_213086_1_) ->
		{
			this.minecraft.setScreen(new JoinMultiplayerScreen(this));
		}).bounds(this.width / 2 - 100, y + rowHeight * 1, 200, 20).build());
	}

	private void updatePackList(PackRepository packRepo)
	{
		this.minecraft.options.updateResourcePacks(packRepo);
		this.minecraft.setScreen(this);
	}
}
