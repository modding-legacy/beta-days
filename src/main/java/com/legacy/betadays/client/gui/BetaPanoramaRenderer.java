package com.legacy.betadays.client.gui;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.PanoramaRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.ARGB;

/**
 * Easiest way to target all the menu related stuff
 */
public class BetaPanoramaRenderer extends PanoramaRenderer
{
	private static final ResourceLocation BACKGROUND_TEXTURE = ResourceLocation.withDefaultNamespace("textures/block/dirt.png");

	public BetaPanoramaRenderer()
	{
		super(null);
	}

	@Override
	public void render(GuiGraphics guiGraphics, int width, int height, float fade, float partialTick)
	{
		float shade = 0.3F;
		var color = ARGB.colorFromFloat(1.0F, shade, shade, shade);
		guiGraphics.blit(RenderType::guiTextured, BACKGROUND_TEXTURE, 0, 0, 0.0F, 0.0F, width, height, 32, 32, color);

	}
}
