package com.legacy.betadays.client.layer;

import com.legacy.betadays.BDConfig;
import com.legacy.betadays.BetaDays;

import net.minecraft.SharedConstants;
import net.minecraft.client.DeltaTracker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.LayeredDraw;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;

public class OldVersionTitleLayer implements LayeredDraw.Layer
{
	public static final ResourceLocation OLD_VERSION_TITLE = BetaDays.locate("beta_days_old_version");

	@Override
	public void render(GuiGraphics graphics, DeltaTracker deltaTracker)
	{
		Minecraft mc = Minecraft.getInstance();
		if (BDConfig.CLIENT.displayIngameVersion() && !mc.options.hideGui && !mc.getDebugOverlay().showDebugScreen())
		{
			Component component = getVersionName(mc);
			int i = mc.font.width(component);

			graphics.drawStringWithBackdrop(mc.font, component, 2, 2, i, -1);
		}
	}

	public static Component getVersionName(Minecraft mc)
	{
		String releaseType = mc.getVersionType();
		return Component.literal("Minecraft " + (releaseType.substring(0, 1).toUpperCase() + releaseType.substring(1)) + " " + SharedConstants.getCurrentVersion().getName());
	}

}
