package com.legacy.betadays.client;

import javax.annotation.Nullable;

import com.legacy.betadays.BDConfig;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.ReceivingLevelScreen.Reason;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;

public class DimensionChangeTexts
{
	private static final Component ENTER_NETHER = buildKey("enterNether"), LEAVE_NETHER = buildKey("leaveNether");
	private static final Component ENTER_END = buildKey("enterEnd"), LEAVE_END = buildKey("leaveEnd");
	private static ResourceKey<Level> knownLevel = Level.OVERWORLD;

	@SuppressWarnings("resource")
	public static void updateKnownLevel()
	{
		LocalPlayer player;
		if ((player = mc().player) != null)
			knownLevel = player.level().dimension();
	}

	@Nullable
	public static Component getText(ReceivingLevelScreen.Reason reason)
	{
		if (BDConfig.CLIENT.customDimensionMessages())
		{
			boolean exiting = knownLevel != Level.OVERWORLD;
			if (reason == Reason.NETHER_PORTAL)
				return exiting ? LEAVE_NETHER : ENTER_NETHER;
			else if (reason == Reason.END_PORTAL)
				return exiting ? LEAVE_END : ENTER_END;
		}

		return null;
	}

	private static final Component buildKey(String key)
	{
		return Component.translatable("beta.menu." + key);
	}

	private static final Minecraft mc()
	{
		return Minecraft.getInstance();
	}

}
