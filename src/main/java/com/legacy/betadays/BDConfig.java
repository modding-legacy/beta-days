package com.legacy.betadays;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;

public class BDConfig
{
	public static void init()
	{

	}

	public static final ModConfigSpec CLIENT_SPEC;
	public static final ModConfigSpec SERVER_SPEC;
	public static final ModConfigSpec STARTUP_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;
	public static final StartupConfig STARTUP;

	static
	{
		{
			final Pair<ClientConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
		{
			final Pair<StartupConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(StartupConfig::new);
			STARTUP = pair.getLeft();
			STARTUP_SPEC = pair.getRight();
		}
	}

	public static class ClientConfig
	{
		private final ModConfigSpec.ConfigValue<Boolean> oldIngameVersion;

		private final ModConfigSpec.ConfigValue<Boolean> disableNetherFog;
		private final ModConfigSpec.ConfigValue<Boolean> customDimensionMessages;

		private final ModConfigSpec.ConfigValue<Boolean> enableClassicMenu;
		private final ModConfigSpec.ConfigValue<Boolean> disableCombatSounds;

		public ClientConfig(ModConfigSpec.Builder builder)
		{
			oldIngameVersion = builder.translation(key("old_version_display")).comment("Displays your Minecraft version at the top of screen when in game.").define("oldIngameVersion", false);

			disableNetherFog = builder.translation(key("disable_nether_fog")).comment("Removes the dense environmental fog from the Nether dimension, pushing back to the end of render distance").define("disableNetherFog", false);
			customDimensionMessages = builder.translation(key("custom_dimension_messages")).comment("Enable custom dimension entry messages. (Vanilla Dimensions)").define("customDimensionMessages", false);
			enableClassicMenu = builder.translation(key("classic_menu")).gameRestart().comment("Enable the classic menu. (May cause mod compatibility issues when used with other menu mods)").define("enableClassicMenu", false);
			disableCombatSounds = builder.translation(key("disable_combat_sounds")).comment("Disable the 1.9+ combat sounds.").define("disableCombatSounds", false);
		}

		public boolean displayIngameVersion()
		{
			return this.oldIngameVersion.get();
		}

		public boolean disableNetherFog()
		{
			return this.disableNetherFog.get();
		}

		public boolean customDimensionMessages()
		{
			return this.customDimensionMessages.get();
		}

		public boolean enableClassicMenu()
		{
			return this.enableClassicMenu.get();
		}

		public boolean disableCombatSounds()
		{
			return this.disableCombatSounds.get();
		}

		public static String key(String key)
		{
			return translationKey("client") + "." + key;
		}
	}

	public static class ServerConfig
	{
		private final ModConfigSpec.ConfigValue<Boolean> disableCombatCooldown;
		private final ModConfigSpec.ConfigValue<Boolean> hungerDisabled;
		private final ModConfigSpec.ConfigValue<Boolean> disableSprinting;
		private final ModConfigSpec.ConfigValue<Boolean> disableSwimming;
		private final ModConfigSpec.ConfigValue<Boolean> originalBow;
		private final ModConfigSpec.ConfigValue<Boolean> disableExperienceDrop;
		private final ModConfigSpec.ConfigValue<Boolean> tillSeeds;
		private final ModConfigSpec.ConfigValue<Boolean> disableCombatSweep;

		public ServerConfig(ModConfigSpec.Builder builder)
		{
			disableCombatCooldown = builder.translation(key("disable_combat_cooldown")).comment("Disables the 1.9+ combat cooldown. (This also decreases axe damage)").define("disableCombatCooldown", false);
			hungerDisabled = builder.translation(key("disable_hunger")).comment("Disables hunger, food gives health instead. (Hides the hunger bar, putting armor in its place)").define("hungerDisabled", false);
			disableSprinting = builder.translation(key("disable_sprinting")).comment("Disables sprinting.").define("disableSprinting", false);
			disableSwimming = builder.translation(key("disable_swimming")).comment("Disables swimming.").define("disableSwimming", false);
			originalBow = builder.translation(key("instant_bow")).comment("Allows for instantly shooting bows.").define("originalBow", false);
			disableExperienceDrop = builder.translation(key("disable_experience")).comment("Disables mobs dropping experience. (Hides the experience bar along with it)").define("disableExperienceDrop", false);
			tillSeeds = builder.translation(key("seed_tilling")).comment("Allows for seeds to drop randomly when tilling dirt.").define("tillSeeds", false);
			disableCombatSweep = builder.translation(key("disable_sword_sweep")).comment("Disables the 1.9+ sweep attack. Only applies when the Sweeping Edge enchantment is not used.").define("disableCombatSweep", false);
		}

		public boolean disableCombatCooldown()
		{
			return this.disableCombatCooldown.get();
		}

		public boolean hungerDisabled()
		{
			return this.hungerDisabled.get();
		}

		public boolean disableSprinting()
		{
			return this.disableSprinting.get();
		}

		public boolean disableSwimming()
		{
			return this.disableSwimming.get();
		}

		public boolean originalBow()
		{
			return this.originalBow.get();
		}

		public boolean disableExperienceDrop()
		{
			return this.disableExperienceDrop.get();
		}

		public boolean tillSeeds()
		{
			return this.tillSeeds.get();
		}

		public boolean disableCombatSweep()
		{
			return this.disableCombatSweep.get();
		}

		public static String key(String key)
		{
			return translationKey("server") + "." + key;
		}
	}

	public static class StartupConfig
	{
		private final ModConfigSpec.ConfigValue<Boolean> disableFoodStacking;
		private final ModConfigSpec.ConfigValue<Boolean> makeFoodStorageUnstackable;

		public StartupConfig(ModConfigSpec.Builder builder)
		{
			disableFoodStacking = builder.translation(key("disable_food_stacking")).gameRestart().comment("Disables the ability to stack food items. When enabled, this also allows you to instantly eat food.").define("disableFoodStacking", false);
			makeFoodStorageUnstackable = builder.translation(key("disable_food_storage_stacking")).gameRestart().comment("Prevents anything in the #beta_days:food_storage_unstackables tag from stacking.").define("makeFoodStorageUnstackable", false);
		}

		public boolean disableFoodStacking()
		{
			return this.disableFoodStacking.get();
		}

		public boolean makeFoodStorageUnstackable()
		{
			return this.makeFoodStorageUnstackable.get();
		}

		public static String key(String key)
		{
			return translationKey("startup") + "." + key;
		}
	}

	public static String translationKey(String key)
	{
		return BetaDays.MODID + "." + key;
	}
}
