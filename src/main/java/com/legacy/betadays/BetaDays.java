package com.legacy.betadays;

import com.legacy.betadays.client.BDClientEvents;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.client.gui.ConfigurationScreen;
import net.neoforged.neoforge.client.gui.IConfigScreenFactory;
import net.neoforged.neoforge.common.NeoForge;

@Mod(BetaDays.MODID)
public class BetaDays
{
	public static final String MODID = "beta_days";

	public static ResourceLocation locate(String key)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, key);
	}

	public static String find(String key)
	{
		return MODID + ":" + key;
	}

	public BetaDays(IEventBus modBus, ModContainer modContainer)
	{
		modContainer.registerConfig(ModConfig.Type.STARTUP, BDConfig.STARTUP_SPEC);
		modContainer.registerConfig(ModConfig.Type.SERVER, BDConfig.SERVER_SPEC);

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			modContainer.registerConfig(ModConfig.Type.CLIENT, BDConfig.CLIENT_SPEC);

			NeoForge.EVENT_BUS.register(BDClientEvents.class);
			modBus.addListener(BDClientEvents::registerOverlays);
		}

		modContainer.registerExtensionPoint(IConfigScreenFactory.class, ConfigurationScreen::new);

		modBus.addListener(BetaDays::commonInit);
		modBus.addListener(EventPriority.LOWEST, BDEvents::modifyDefaultComponents);
	}

	public static void commonInit(FMLCommonSetupEvent event)
	{
		NeoForge.EVENT_BUS.register(BDEvents.class);
	}
}